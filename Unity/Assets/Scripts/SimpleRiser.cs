﻿using UnityEngine;
using System.Collections;

public class SimpleRiser : MonoBehaviour {

    public float riseSpeed, riseVariance, wiggleVariance;
    private float wiggle, wiggleCounter;

	// Use this for initialization
	void Start () {
        riseSpeed += Random.Range(-riseVariance, riseVariance);
        wiggleCounter = .5f;
	}
	
	// Update is called once per frame
	void Update () {
        if (wiggleCounter <= 0f)
        {
            wiggle += Random.Range(-wiggleVariance, wiggleVariance);
            wiggleCounter = .5f;
        }
        else
        {
            wiggleCounter -= Time.deltaTime;
        }
        Vector2 position = transform.position;
        Vector2 velocity = new Vector2(wiggle, riseSpeed);

        position += velocity * Time.deltaTime;

        transform.position = position;
	}
}
