﻿using UnityEngine;
using System.Collections;

public class Flipper : MonoBehaviour {

    public float rotateSpeed = 5f;

    private bool isFlipped;
    private float flipOffset;

	// Use this for initialization
	void Start () 
    {
        flipOffset = UnityEngine.Random.Range(-10f, 10f);
	}
	
	// Update is called once per frame
	void Update () {
        float angle = Mathf.Abs(Mathf.Atan2(transform.up.y, transform.up.x));

        if (angle > Mathf.Deg2Rad * (135 + flipOffset))
        {
            isFlipped = true;
        }
        else if (angle < Mathf.Deg2Rad * (45 +  flipOffset))
        {
            isFlipped = false;
        }

        if (isFlipped)
        {
            if (transform.localScale.x > -1f)
            {
                float augment = rotateSpeed * Time.deltaTime;
                Vector3 scale = transform.localScale;
                scale.x = Mathf.Max(scale.x - augment, -1f);
                transform.localScale = scale;
            }
        }
        else if (transform.localScale.x < 1f)
        {
            float augment = rotateSpeed * Time.deltaTime;
            Vector3 scale = transform.localScale;
            scale.x = Mathf.Min(scale.x + augment, 1f);
            transform.localScale = scale;
        }

	}
}
