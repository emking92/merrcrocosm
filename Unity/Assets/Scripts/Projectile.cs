﻿using UnityEngine;
using System.Collections;
using System;

public class Projectile : MonoBehaviour {

    public float speed, life;
    public int power;
    public Vector2 velocity;

    public Action OnDestroy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 position = transform.position2d();

        position += velocity * Time.deltaTime * speed;

        transform.position = position;

        if (life > 0f)
        {
            life -= Time.deltaTime;
        }
        else
        {
            DestroyThis();
        }
	}

    private void DealDamage(GameObject other)
    {
        if (other.GetComponent<Enemy>() != null)
            other.GetComponent<Enemy>().ApplyDamage(power);
        else if (other.GetComponent<Player_Controller>() != null)
            other.GetComponent<Player_Controller>().Take_Damage(power);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.GetComponent<Enemy>() != null || coll.gameObject.GetComponent<Player_Controller>() != null)
        {
            DealDamage(coll.gameObject);
        }

        if (OnDestroy != null)
        {
            OnDestroy();
        }
        DestroyThis();
    }

    private void DestroyThis()
    {
        GameObject.Destroy(this.gameObject);
    }
}
