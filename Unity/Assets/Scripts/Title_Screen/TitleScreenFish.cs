﻿using UnityEngine;
using System.Collections;

public class TitleScreenFish : MonoBehaviour {
	
	public float roamSpeed;
	public float aggroSpeed;
	public float aggroProximity;
	public float speedVariance;
	
	private KinematicMover kinematicMover;
	private Vector2 roamTarget;
	private bool isAggroed;
	
	void Awake()
	{
		kinematicMover = this.GetComponent<KinematicMover>();
		roamSpeed += UnityEngine.Random.Range(-speedVariance, speedVariance);
		aggroSpeed += UnityEngine.Random.Range(-speedVariance, speedVariance);
		
	}
	
	// Use this for initialization
	void Start () 
	{
		ChooseRoamTarget();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector2 target;

		if (kinematicMover.HasArrived)
		{
			ChooseRoamTarget();
		}
		target = roamTarget;
		kinematicMover.Speed = roamSpeed;

		kinematicMover.MoveTowardsTarget(target);
	}
	
	private void ChooseRoamTarget()
	{
		roamTarget = Water.Bounds.RandomInnerPoint();
	}
	
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, aggroProximity);
	}
}
