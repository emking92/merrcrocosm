﻿using UnityEngine;
using System.Collections;

public class TitleScreenCrab : MonoBehaviour {
	
	public float max_movement_speed = 2;
	public float max_aggro_speed = 3.5f;
	private int dir = 1;
	
	private ColliderCheck right_check;
	private ColliderCheck left_check;
	
	public float x_variation = .2f;
	
	//Square aggro-distance
	public float aggro = 25;
	private bool is_aggro = false;

	private float jump_delay = 5;
	private float jump_timer = 0;

	private Vector2 player_position;
	
	public GameObject body, claws;
	private Animator bodyAnimator, clawsAnimator;
	
	// Use this for initialization
	void Start () {
		
	}
	
	void Awake(){
		right_check = transform.FindChild ("Right_Wall_Check").GetComponent<ColliderCheck> ();
		left_check = transform.FindChild ("Left_Wall_Check").GetComponent<ColliderCheck> ();
		
		bodyAnimator = body.GetComponent<Animator>();
		clawsAnimator = claws.GetComponent<Animator>();
		bodyAnimator.SetBool("isWalkingLeft", true);
		clawsAnimator.SetBool("isWalkingLeft", true);
	}
	
	void FixedUpdate(){
		if(is_aggro){
			rigidbody2D.CapSpeed(max_aggro_speed);
		}
		else{
			rigidbody2D.CapSpeed(max_movement_speed);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(right_check.IsColliding && dir == 1){
			rigidbody2D.velocity = Vector2.zero;
			dir = -1;
		}
		else if(left_check.IsColliding && dir == -1){
			rigidbody2D.velocity = Vector2.zero;
			dir = 1;
		}

		rigidbody2D.AddForce(new Vector2(4f * dir, 0));
		if (dir < 0)
		{
			bodyAnimator.SetBool("isWalkingLeft", true);
			clawsAnimator.SetBool("isWalkingLeft", true);
		}
		else
		{
			bodyAnimator.SetBool("isWalkingLeft", false);
			clawsAnimator.SetBool("isWalkingLeft", false);
		}
		if(jump_timer >= jump_delay){
			if(Random.Range(0, 60) == 15){
				jump_timer = 0f;
				jump_delay = Random.Range(5, 12);
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Random.Range(4000, 5500));
			}
		}
		else{
			jump_timer += Time.deltaTime;
		}
	}
	
	public void OnShot()
	{
		clawsAnimator.SetTrigger("attack");
	}
}
