﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

    public int ammo;
    public float fireDelay;
	public float fireDelayMultiplier;
	public int NumberShots = 1;
    public int Spread = 5;
	public int power = 5;
    public GameObject projectilePrefab;
        
    private float fireTimer;
    
	// Use this for initialization
	void Start ()
    {
        fireTimer = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
        fireTimer -= Time.deltaTime;
	}

    public bool fireShot(Vector2 direction)
    {
        return fireShot(direction, 1);
    }

	public bool fireShot(Vector2 direction, float strength_multiplier)
    {
        if (fireTimer <= 0)
        {
            Quaternion rotation = Quaternion.LookRotation(Vector3.forward, direction);

            fireTimer = fireDelay * fireDelayMultiplier;
			GameObject projectileObject;
			Projectile projectile;

	        projectileObject = GameObject.Instantiate(projectilePrefab, transform.position, transform.rotation) as GameObject;
	        projectile = projectileObject.GetComponent<Projectile>();
			projectile.power =(int) (power * strength_multiplier);
			projectile.velocity = direction;
            projectile.transform.rotation = rotation;

			for(int e = 1; e < (NumberShots); e += 2)
            {
				projectileObject = GameObject.Instantiate(projectilePrefab, transform.position, transform.rotation) as GameObject;
				projectile = projectileObject.GetComponent<Projectile>();
				projectile.power =(int) (power * strength_multiplier);
                projectile.velocity = Quaternion.Euler(0, 0, -Spread * e) * direction;
                projectile.transform.rotation = rotation;

				projectileObject = GameObject.Instantiate(projectilePrefab, transform.position, transform.rotation) as GameObject;
				projectile = projectileObject.GetComponent<Projectile>();
				projectile.power =(int) (power * strength_multiplier);
                projectile.velocity = Quaternion.Euler(0, 0, Spread * e) * direction;
                projectile.transform.rotation = rotation;
			}

            return true;
        }
        return false;
    }
}
