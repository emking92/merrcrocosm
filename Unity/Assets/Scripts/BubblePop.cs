﻿using UnityEngine;
using System.Collections;

public class BubblePop : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Projectile>().OnDestroy = createBubbles;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void createBubbles()
    {
		Sound.current.Play_Sound (1);

        for (int i = 0; i < 3; i++)
        {
            GameObject particleObject = ResourceFactory<Particles>.Instantiate(Particles.RisingBubble, transform.position);
            float x = UnityEngine.Random.Range(-.25f, .25f);
            float y = UnityEngine.Random.Range(-.25f, .25f);
            particleObject.transform.position += new Vector3(x, y, 0f);
            Destroy(particleObject, Random.Range(.5f, 1f));
        }
    }
}
