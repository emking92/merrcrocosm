﻿using UnityEngine;
using System.Collections;

public class JellyfishEnemy : MonoBehaviour
{
    public float xSpeed, jumpSpeed, maxSpeed, damageRate;
    public int periodicDamage;

    private float horizontalCounter, damageCounter;
    public bool attached;

    // Use this for initialization
    void Start()
    {

    }

    void FixedUpdate()
    {
        rigidbody2D.CapSpeed(maxSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        if (!attached){

            Vector2 playerPosition = Player_Controller.current.transform.position2d();
            Vector2 velocity = rigidbody2D.velocity;

            if (horizontalCounter <= 1f)
            {
                float xDec;
                if (velocity.x > 0)
                {
                    xDec = -1f;
                }
                else
                {
                    xDec = 1f;
                }
                velocity.x += xDec * Time.deltaTime;
                rigidbody2D.velocity = velocity;
            }
            

            if (horizontalCounter <= 0f)
            {
                if (playerPosition.y - 2f >= transform.position.y)
                {
                    float xDistance = playerPosition.x - transform.position.x;
                    if (xDistance > 1f)
                    {
                        xDistance = 1f;
                    }
                    if (xDistance < -1f)
                    {
                        xDistance = -1f;
                    }

                    float yDistance = playerPosition.y - transform.position.y;
                    float ySpeed = jumpSpeed * yDistance;
                    
                    velocity.Set(xDistance * xSpeed, ySpeed);

                    rigidbody2D.velocity = velocity;
                    horizontalCounter = 2f;
                }
            }

            horizontalCounter -= Time.deltaTime;
        }
        else
        {
            //transform.localPosition = Vector3.zero;
            if (damageCounter <= 0f)
            {
                damageCounter = damageRate;
                Player_Controller.current.Take_Damage(periodicDamage);
            }
            else
            {
                damageCounter -= Time.deltaTime;
            }

        }
    }

    void LateUpdate()
    {
        if (attached)
        {
            transform.position = Player_Controller.current.transform.position;
        }
    }

    

    public void OnDeath()
    {
        if (attached)
        {
            Player_Controller.current.max_speed_multiplier = 1f;
        }
    }
}
