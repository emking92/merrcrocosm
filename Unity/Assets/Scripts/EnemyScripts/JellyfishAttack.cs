﻿using UnityEngine;
using System.Collections;

public class JellyfishAttack : MonoBehaviour {

    JellyfishEnemy mainScript;
	// Use this for initialization
	void Start () {
        mainScript = transform.parent.GetComponent<JellyfishEnemy>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player" && !mainScript.attached)
        {
            mainScript.attached = true;
            rigidbody2D.gravityScale = 0f;
            rigidbody2D.velocity = new Vector2(0f, 0f);
            transform.localPosition = Vector3.zero;
            Player_Controller.current.max_speed_multiplier = .5f;
        }
    }
}
