﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour 
{
    public int Health;
	private int max_health;

    private Collider2D coll;
    private bool isSpawning, hit;
    private Vector2 spawnStart;
    private Vector2 spawnDirection;
    private float spawnProgress;
    private float spawnDistance;
    private float hitTimer;

    private const float SPAWN_SPEED = 1;

    private SpriteRenderer spriteRenderer;

    private List<SpriteRenderer> rendererList;

    public GameObject skeleton;

    void Start()
    {
        rendererList = new List<SpriteRenderer>();
        rendererList.AddRange(GetComponentsInChildren<SpriteRenderer>());
		max_health = Health;
    }


    void FixedUpdate()
    {
        if (isSpawning)
        {
            float delta = SPAWN_SPEED * Time.deltaTime;
            spawnProgress += delta;

            transform.position = spawnStart + spawnDirection * spawnProgress;

            if (spawnProgress >= spawnDistance)
            {
                rigidbody2D.isKinematic = false;
                coll.enabled = true;
                isSpawning = false;
            }
        }
    }

    void Update()
    {
        if (hitTimer > 0f)
        {
            hitTimer -= Time.deltaTime;
        }
        else
        {
            foreach (SpriteRenderer renderer in rendererList)
            {
                renderer.color = new Vector4(1f, 1f, 1f, 1f);
            }
        }
    }

    public void ApplyDamage(int damage)
    {
		Sound.current.Play_Sound (4);
        Health -= damage;

        hitTimer = .2f;
        foreach (SpriteRenderer renderer in rendererList)
        {
            renderer.color = new Vector4(1f, 0f, 0f, 1f);
        }

        if (Health <= 0)
            Die();
    }

    public void Spawn(Vector2 direction, float distance)
    {
        isSpawning = true;
        spawnStart = transform.position;
        spawnDirection = direction;
        spawnProgress = 0;
        spawnDistance = distance;

        coll = collider2D;
        coll.enabled = false;
        rigidbody2D.isKinematic = true;
    }

    private void Die()
    {
		Sound.current.Play_Sound (2);
        SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
        CollectibleManager.current.OnEnemyDeath(transform.position);
		HUD.current.Add_Score (max_health);
        

        if (skeleton != null)
        {
            GameObject obj = GameObject.Instantiate(skeleton, transform.position, transform.rotation) as GameObject;
            Vector3 localScale = transform.localScale;
            if (localScale.x < 0f)
            {
                localScale.x = -1f;
            }
            else
            {
                localScale.x = localScale.y;
            }
            obj.transform.localScale = localScale;
        }
        GameObject.Destroy(this.gameObject);
    }
}

public enum Enemies
{
    Guppy,
    Eel,
    Crab,
    SmallCrab
}