﻿using UnityEngine;
using System.Collections;

public class RevolvingEnemy : MonoBehaviour 
{
    public float NormalDistance;
    public float MaxDistance;
    public float MinDistance;
    public float DistanceVariance;
    
    private float AngleInterval = 10f;
    private float DistanceInterval = .25f;

    private KinematicMover kinematicMover;

	// Use this for initialization
	void Awake () 
    {
        kinematicMover = GetComponent<KinematicMover>();
        MaxDistance += UnityEngine.Random.Range(-DistanceVariance, DistanceVariance);
        NormalDistance += UnityEngine.Random.Range(-DistanceVariance, DistanceVariance);
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector2 playerPosition = Player_Controller.current.transform.position;
        Vector2 targetDisplacement = (playerPosition - transform.position2d());

        float magnitude = targetDisplacement.magnitude;

        if (magnitude > MaxDistance)
        {
            kinematicMover.MoveTowardsTarget(playerPosition);
        }
        else
        {
            float distance = Mathf.Min(magnitude + DistanceInterval, NormalDistance);

            float angle = Mathf.Atan2(-targetDisplacement.y, -targetDisplacement.x);
            float angleAdjust = AngleInterval * Mathf.PI / 180f * Mathf.Sign(Vector3.Cross(transform.up, targetDisplacement).z);
            angle += angleAdjust;

            Vector2 target;

            do
            {
                target = pointFromPlayer(playerPosition, angle, distance);
                distance -= DistanceInterval;
            } while (!Water.Bounds.Contains(target) && distance > MinDistance);
            if (distance <= MinDistance)
            {
                target = Vector2.zero;
                kinematicMover.StopMovement();
            }

            Debug.DrawLine(transform.position, target, Color.green);
            kinematicMover.MoveTowardsTarget(target);
        }
	}

    private Vector2 pointFromPlayer(Vector2 playerPosition, float angle, float distance)
    {
        Vector2 direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        return playerPosition + (direction * distance);   
    }

    private void OnDrawGizmos()
    {
        if (Application.isEditor)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(Vector2.zero, NormalDistance);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(Vector2.zero, MaxDistance);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(Vector2.zero, MinDistance);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(Player_Controller.current.transform.position, NormalDistance);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(Player_Controller.current.transform.position, MaxDistance);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(Player_Controller.current.transform.position, MinDistance);

        }
    }
}
