﻿using UnityEngine;
using System.Collections;

public class RangedEnemy : MonoBehaviour 
{
    public float range;

    private float rangeSqr;
    private Gun gun;
    private Animator animator;
    
	// Use this for initialization
	void Awake () 
    {
        rangeSqr = range * range;
        gun = transform.FindChild("Gun").GetComponent<Gun>();
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector2 displacement = (Player_Controller.current.transform.position - transform.position);
        if (displacement.sqrMagnitude < rangeSqr)
        {
            if (gun.fireShot(displacement.normalized))
            {
                SendMessage("OnShot", SendMessageOptions.DontRequireReceiver);
                if (animator != null)
                    animator.SetTrigger("Attack");
            }
        }
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
