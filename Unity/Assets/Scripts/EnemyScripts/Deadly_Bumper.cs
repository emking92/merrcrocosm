﻿using UnityEngine;
using System.Collections;

public class Deadly_Bumper : MonoBehaviour {

	public int damage = -10;

	void Start(){

	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) 
    {
        Trigger(other);
	}

    void OnTriggerStay2D(Collider2D other)
    {
        Trigger(other);
    }

    private void Trigger(Collider2D other)
    {
        Player_Controller player = other.GetComponent<Player_Controller>();
        if (player != null)
        {
            player.Take_Damage(damage);
        }
    }
}
