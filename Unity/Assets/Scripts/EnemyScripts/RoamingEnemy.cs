﻿using UnityEngine;
using System.Collections;

public class RoamingEnemy : MonoBehaviour {

    public float roamSpeed;
    public float aggroSpeed;
    public float aggroProximity;
    public float speedVariance;

    private KinematicMover kinematicMover;
    private Vector2 roamTarget;
    private float aggroProximitySqr;
    private bool isAggroed;

    void Awake()
    {
        kinematicMover = this.GetComponent<KinematicMover>();
        roamSpeed += UnityEngine.Random.Range(-speedVariance, speedVariance);
        aggroSpeed += UnityEngine.Random.Range(-speedVariance, speedVariance);

    }

	// Use this for initialization
	void Start () 
    {
        aggroProximitySqr = aggroProximity * aggroProximity;
        ChooseRoamTarget();
	}

	// Update is called once per frame
	void Update () 
    {
        Vector2 target;
        Vector2 playerPosition = Player_Controller.current.transform.position2d();

        if (isAggroed)
        {
            target = playerPosition;
            kinematicMover.Speed = aggroSpeed;
            if (kinematicMover.HasArrived)
            {
                kinematicMover.StopMovement();
            }
        }
        else if ((playerPosition - transform.position2d()).sqrMagnitude < aggroProximitySqr)
        {
            isAggroed = true;
            target = playerPosition;
            kinematicMover.Speed = aggroSpeed;
        }
        else
        {
            if (kinematicMover.HasArrived)
            {
                ChooseRoamTarget();
            }
            target = roamTarget;
            kinematicMover.Speed = roamSpeed;
        }

        kinematicMover.MoveTowardsTarget(target);
	}

    private void ChooseRoamTarget()
    {
        roamTarget = Water.Bounds.RandomInnerPoint();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, aggroProximity);
    }
}
