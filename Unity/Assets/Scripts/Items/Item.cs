﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Item : MonoBehaviour {

	public bool permanent;
    public int weaponCount;

    public Sprite Icon
    {
        get
        {
            return GetComponent<SpriteRenderer>().sprite;
        }
    }

    public abstract bool Use(Transform transform);
}

public enum Items
{
    [ItemOdds(2)] Blowfish,
    [ItemOdds(3)] Dash,
    [ItemOdds(2)] Starfish,
    [ItemOdds(2)] Multishot,
    [ItemOdds(4)] FireRateMultiplier,
    [ItemOdds(8)] Heal1,
    [ItemOdds(3)] Heal2
}

public class ItemOddsAttribute : Attribute
{
    public int Odds;
    public ItemOddsAttribute(int odds)
    {
        Odds = odds;
    }
}