﻿using UnityEngine;
using System.Collections;

public class Friendly_Bumper : MonoBehaviour 
{

    public int power;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.CompareTag("Enemy")){
			other.GetComponent<Enemy>().ApplyDamage(power);
		}
		else if(other.CompareTag("Projectile")){
			Destroy(other.gameObject);
		}
	}
}
