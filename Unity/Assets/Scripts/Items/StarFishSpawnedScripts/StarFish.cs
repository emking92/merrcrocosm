﻿using UnityEngine;
using System.Collections;

public class StarFish : MonoBehaviour {

    private float currentAngles;
    public float rotationSpeed;
    public int power;

    // Use this for initialization
    void Start()
    {
        currentAngles = transform.GetRightAngle();
    }
	
	// Update is called once per frame
    void LateUpdate()
    {
        currentAngles += rotationSpeed * Time.deltaTime;
        transform.SetRightAngle(currentAngles);
    }

    private void DealDamage(GameObject other)
    {
        if (other.GetComponent<Enemy>() != null)
            other.GetComponent<Enemy>().ApplyDamage(power);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.GetComponent<Enemy>() != null)
        {
            DealDamage(coll.gameObject);
        }

        if (coll.tag == "Projectile")
        {
            GameObject.Destroy(coll.gameObject);
        }

        GameObject.Destroy(this.gameObject);
    }
}
