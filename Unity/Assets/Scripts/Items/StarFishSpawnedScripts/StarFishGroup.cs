﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarFishGroup : MonoBehaviour {

    private float currentAngles;
    public float rotationSpeed;
	// Use this for initialization
	void Start () {
        currentAngles = transform.GetRightAngle();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = Player_Controller.current.transform.position;
        currentAngles += rotationSpeed * Time.deltaTime;
        transform.SetRightAngle(currentAngles);

        if (transform.childCount == 0)
        {
            Destroy(this.gameObject);
        }
	}
}
