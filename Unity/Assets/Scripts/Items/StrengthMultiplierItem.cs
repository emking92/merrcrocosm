﻿using UnityEngine;
using System.Collections;

public class StrengthMultiplierItem : Item {

    public float strengthAdjust = .2f;

	public override bool Use(Transform transform)
	{
        if (Player_Controller.current.strength_multiplier >= 2) return false;

		Player_Controller.current.strength_multiplier += strengthAdjust;

        return false;
	}
}