﻿using UnityEngine;
using System.Collections;

public class MaxSpeedMultiplier : Item {

	public override bool Use(Transform transform)
	{
		Player_Controller.current.max_speed_multiplier += .2f;

        return false;
	}
}
