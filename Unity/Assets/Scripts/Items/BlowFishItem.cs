﻿using UnityEngine;
using System.Collections;

public class BlowFishItem : Item {

    public GameObject projectilePrefab;
    public int power;
    public int speed;
    public int count;

    public override bool Use(Transform transform)
    {
        for (int angle = 0; angle < 360; angle += 360/count)
        {
            Vector2 direction = Quaternion.Euler(0, 0, angle) * Vector2.up;
            Quaternion rotation = Quaternion.LookRotation(Vector3.forward, direction);

            GameObject projectileObject = GameObject.Instantiate(projectilePrefab, transform.position, transform.rotation) as GameObject;
            Projectile projectile = projectileObject.GetComponent<Projectile>();
            projectile.velocity = direction;
            projectile.power = power;
            projectile.speed = speed;
            projectile.transform.rotation = rotation;
        }

        return false;
    }
}