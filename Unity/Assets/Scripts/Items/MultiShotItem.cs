﻿using UnityEngine;
using System.Collections;

public class MultiShotItem : Item 
{
    public int NumberShots;
    public float Duration;

	public override bool Use(Transform transform)
	{
		Player_Controller.current.Set_Multi_Shot(NumberShots, Duration);

        return false;
	}
}
