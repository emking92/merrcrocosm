﻿using UnityEngine;
using System.Collections;

public class HealItem : Item {

    public int value;

	public override bool Use(Transform transform)
	{
        Player_Controller.current.Take_Damage(-value);
        return false;
	}
}
