﻿using UnityEngine;
using System.Collections;

public class FireRateMultiplierItem : Item {

    public float fireRateAdjust = 1.2f;
    public float duration = 5;

	public override bool Use(Transform transform)
	{
		Player_Controller.current.Change_Fire_Rate(fireRateAdjust, duration);

        return false;
	}
}
