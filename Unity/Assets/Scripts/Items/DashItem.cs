﻿using UnityEngine;
using System.Collections;

public class DashItem : Item {

	public GameObject dashPrefab;
    public int power;
    public float dashTime = .25f;

	public override bool Use(Transform transform)
	{
		GameObject friendly_bumber = GameObject.Instantiate(dashPrefab, transform.position, transform.rotation) as GameObject;
        friendly_bumber.GetComponent<Friendly_Bumper>().power = power;
		friendly_bumber.SetParentObject(Player_Controller.current.gameObject);
		Destroy(friendly_bumber, .15f);
		Player_Controller.current.Dash (dashTime);
		return false;
	}
}
