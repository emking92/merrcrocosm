﻿using UnityEngine;
using System.Collections;

public class StarFishItem : Item {

    public GameObject starfishPrefab;

    public override bool Use(Transform transform)
    {
        GameObject starfishObject = GameObject.Instantiate(starfishPrefab, transform.position, transform.rotation) as GameObject;

		return false;
    }
}
