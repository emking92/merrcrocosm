﻿using UnityEngine;
using System.Collections;

public class Player_Controller : MonoBehaviour {
	public static Player_Controller current;
	//Health Stats
	public int max_health = 8;
	public int health = 8;
	public bool dead = false;

	//Movement Stats
	public float max_speed = 2;
	public float acceleration = 20;
	//Movement Upgrades
	public float max_speed_multiplier = 1f;

	//Gun Stats
	public float strength_multiplier = 1f;
	public float firedelay_multiplier = 1f;
	public int multi_shot = 1;
    public float multiShotTimer;

    public float fireRateTimer;

	public bool dash = false;
	private float dash_timer;

    public Gun gun;

    private SpriteRenderer itemHolderRenderer;
    public Item currentItem;
    public bool usingItem;
    public float invincibilityTime;

    private Animator animator;
    private float hitCounter, invincibilityTimer;

    void Awake()
    {
        current = this;
        animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start ()
    {
        itemHolderRenderer = GameObject.Find("ItemHolder").GetComponent<SpriteRenderer>();
		gun.fireDelayMultiplier = firedelay_multiplier;
		//HUD.current.setHealth (health);
	}

    void FixedUpdate()
    {
		if(!dash)
        {
        	rigidbody2D.CapSpeed(max_speed * max_speed_multiplier);
		}
		else
        {
			rigidbody2D.CapSpeed(max_speed * max_speed_multiplier * 6);
		}
    }
	
	// Update is called once per frame
	void Update () 
    {
		if(!dead){

            multiShotTimer -= Time.deltaTime;
            if (multiShotTimer < 0)
            {
                gun.NumberShots = 1;
            }

            fireRateTimer -= Time.deltaTime;
            if (fireRateTimer < 0)
            {
                gun.fireDelayMultiplier = 1;
            }

            HUD.current.transform.position = this.transform.position;
			if(!dash){
				Movement ();
				Rotation ();
		        Weapons();
			}
			else
	        {
				dash_timer -= Time.deltaTime;
				if(dash_timer <= 0)
	            {
					dash = false;
                    invincibilityTimer = 1f;
				}
                if (invincibilityTimer > 0f)
                {
                    invincibilityTimer -= Time.deltaTime;
                }
				Dashing();
			}

	        if (hitCounter > 0)
	        {
	            hitCounter -= Time.deltaTime;
	            float inc = invincibilityTime / 8f;
	            if (hitCounter % inc <= inc/4f)
	            {
	                animator.SetBool("isHit", false);
	            }
	            else
	            {
	                animator.SetBool("isHit", true);
	            }
	        }
	        else
	        {
	            animator.SetBool("isHit", false);
	            hitCounter = 0;
	        }
		}
	}

	public void Dashing()
    {
        rigidbody2D.AddForce(18 * acceleration * transform.up);

	}

	void Rotation()
    {
		Vector2 mouse_pos = InputUtilities.MouseWorldPosition();

        Vector2 dir = mouse_pos - transform.position2d();
            
        transform.SetUpDirection (dir);
	}

	void Movement()
    {
        rigidbody2D.AddForce(acceleration * 
            (Vector2.up * Input.GetAxis("Vertical") + Vector2.right * Input.GetAxis("Horizontal")));

        if (rigidbody2D.velocity.magnitude > 1f)
        {
            animator.SetBool("isWalking", true);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
	}

    void Weapons()
    {
        if (Input.GetMouseButton(0))
        {
            gun.fireShot(transform.up, strength_multiplier);
            animator.SetBool("isAttacking", true);
        }
        else
        {
            animator.SetBool("isAttacking", false);
        }

        if (Input.GetMouseButton(1))
        {
            if (currentItem != null)
            {
                bool check = currentItem.Use(transform);

                if (!check)
                {
                    GameObject.Destroy(currentItem.gameObject);
                    currentItem = null;
                    itemHolderRenderer.sprite = null;
                }
            }
        }
    }

	public void Change_Fire_Rate(float amount, float duration)
    {
        fireRateTimer = duration;
		gun.fireDelayMultiplier = amount;
	}

	public void Set_Multi_Shot(int amount, float duration)
    {
		multi_shot = amount;
        multiShotTimer = duration;
		gun.NumberShots = multi_shot;
	}

	public void Take_Damage(int damage)
    {
		if(!dash && hitCounter == 0 && invincibilityTimer <= 0f)
        {
            if (damage > 0)
            {
                hitCounter = invincibilityTime;
                animator.SetBool("isHit", true);
            }

			Sound.current.Play_Sound (3);
			health -= damage;
	        if (health > max_health)
	        {
	            health = max_health;
	        }

	        if (health <= 0)
            {
                health = 0;
	            Die();
	        }

	        HUD.current.setHealth(health);
		}
	}

	private void Die()
    {
		dead = true;
		//Destroy (this.gameObject);
        //Application.Quit();
	}

	void OnTriggerEnter2D(Collider2D other) 
    {
		Item item = other.GetComponent<Item> ();
		if(item != null)
        {
			if(item.permanent)
            {
				item.Use(transform);
                if (other.transform.parent != null)
                {
                    GameObject.Destroy(other.transform.parent.gameObject);
                }
				Destroy(other.gameObject);
			}
			else
            {
				if(currentItem != null)
                {
					GameObject.Destroy(currentItem.gameObject);
				}
				currentItem = item;
				other.GetComponent<SpriteRenderer>().enabled = false;
				other.enabled = false;

                Transform parent = other.transform.parent;
                if (parent != null)
                {
                    other.transform.parent = null;
                    GameObject.Destroy(parent.gameObject);
                }

                itemHolderRenderer.sprite = item.Icon;
			}
		}
	}

	public void Dash(float time)
    {
		dash = true;
		dash_timer = time;
	}
}
