﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class CollectibleManager : MonoBehaviour 
{
    public static CollectibleManager current;

    public GameObject ItemBubblePrefab;

    public float spawnDelay = 10f;
    public float spawnOdds = .25f;

    private List<Items> items;
    private float spawnTimer;
    

    void Awake()
    {
        current = this;
        items = new List<Items>();

        Type type = typeof(Items);
        foreach (Items item in (Items[]) Enum.GetValues(type))
        {
            var memInfo = type.GetMember(item.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(ItemOddsAttribute), false);
            int odds = ((ItemOddsAttribute)attributes[0]).Odds;
            for (int i = 0; i < odds; i++)
            {
                items.Add(item);
            }
        }

        spawnTimer = spawnDelay;
    }

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        spawnTimer -= Time.deltaTime;
	}

    public void OnEnemyDeath(Vector2 location)
    {
        if (spawnTimer <= 0)
        {
            float roll = UnityEngine.Random.value;
            if (roll < spawnOdds)
            {
                SpawnItem(location);
            }
        }
    }

    private void SpawnItem(Vector2 location)
    {
        spawnTimer = spawnDelay;
        Items item = items.PickRandom();
        GameObject itemObject = ResourceFactory<Items>.Instantiate(item, Vector2.one*1000);

        GameObject bubbleObject = GameObject.Instantiate(ItemBubblePrefab, location, Quaternion.identity) as GameObject;
        
        itemObject.SetParentObject(bubbleObject);
        itemObject.transform.localPosition = Vector2.zero;

        GameObject.Destroy(bubbleObject, 15);
    }
}
