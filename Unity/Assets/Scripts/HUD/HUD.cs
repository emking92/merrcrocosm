﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public static HUD current;

    //private Slider HealthBar;
	private Image healthImage;

	private Text game_over_text;
	private Button game_over_button;
	private Text playagaintext;

	//Gameplay Stats
	public float game_time = 0f;
	private Text time_GUI;
	public int points = 0;
	private Text points_GUI;

	private float alpha_speed = .1f;
	private bool is_visible = false;
	private float fade_delay = 0f;

	private bool death = false;

    void Awake()
    {
        current = this;

    }

	// Use this for initialization
	void Start () {
		healthImage = GameObject.Find ("PlayerHealthImage").GetComponent<Image> ();
        healthImage.color = healthImage.color - new Color(0, 0, 0, healthImage.color.a);

		game_over_text = GameObject.Find ("GameOverText").GetComponent<Text>();
		game_over_button = GameObject.Find ("PlayAgainButton").GetComponent<Button>();
		playagaintext =  GameObject.Find("PlayAgainText").GetComponent<Text>();
		time_GUI = GameObject.Find("Time_Text").GetComponent<Text>();
		points_GUI = GameObject.Find("Points_Text").GetComponent<Text>();

		game_over_text.enabled = false;
		game_over_button.enabled = false;
		game_over_button.image.enabled = false;
		playagaintext.enabled = false;
		time_GUI.enabled = false;
		points_GUI.enabled = false;


	}
	
	// Update is called once per frame
	void Update () {
		if(death){
			//game_over.enabled = true;
		}
		else{
			game_time += Time.deltaTime;
		}

		if(is_visible){
			fade_delay -= Time.deltaTime;
			if(fade_delay <= 0){
				healthImage.color = healthImage.color - new Color(0, 0, 0, alpha_speed * Time.deltaTime);
				if(healthImage.color.a <= .1f){
					healthImage.color = healthImage.color - new Color(0, 0, 0, healthImage.color.a);
					is_visible = false;
				}
			}
		}
	}

	public void Add_Score(int score){
		points += score;
	}

    public void setHealth(int value)
    {
		is_visible = true;
		healthImage.color = (healthImage.color * new Color(1, 1, 1, 0)) + Color.black;
		healthImage.fillAmount = (value / 8f);
		alpha_speed = .5f;;
		fade_delay = .25f;
		if(value == 0){
			death = true;
			game_over_text.enabled = true;
			game_over_button.enabled = true;
			game_over_button.image.enabled = true;
			playagaintext.enabled = true;
			time_GUI.enabled = true;
			points_GUI.enabled = true;
			time_GUI.text = (string) ("You Survived " + (float) (((int)(game_time * 100))) / 100f) + " seconds!!!";
			points_GUI.text = (string) ("You Scored " + points + " points!!!");
		}
    }
}
