﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {

	public static Sound current;
	public AudioSource[] sounds;

	void Awake()
	{
		current = this;
		sounds = this.GetComponents<AudioSource> ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Play_Sound(int sound){
		sounds [sound].Play ();
	}
}
