﻿using UnityEngine;
using System.Collections;
using System;

public class KinematicMover : MonoBehaviour 
{

    public float Speed;
    public float SpeedVariance = .25f;
    public float TargetVariance = .25f;

    public float RotateSpeed;
    public float targetSatisfaction = .1f;
    public float satisfactionAngles = 30;
    public float acceleration = 2;
    public Vector2 targetOffset;

    private bool stopped;

    public bool HasArrived
    {
        get;
        private set;
    }

    private Vector2 target;
    private Action onArrival;
    private float targetSatisfactionSqr;

    void Awake()
    {
        Speed += UnityEngine.Random.Range(-SpeedVariance, SpeedVariance);
        targetOffset = new Vector2(UnityEngine.Random.Range(-TargetVariance, TargetVariance),
                                   UnityEngine.Random.Range(-TargetVariance, TargetVariance));
    }

	void Update ()
    {
        targetSatisfactionSqr = targetSatisfaction * targetSatisfaction;	
	}

    void FixedUpdate()
    {
        rigidbody2D.CapSpeed(Speed);
    }

	// Update is called once per frame
	void LateUpdate () 
    {
        Vector2 targetDirection = target - transform.position2d();

        if (!stopped)
        {
            if (targetDirection.sqrMagnitude < targetSatisfactionSqr)
            {
                rigidbody2D.velocity = Vector2.zero;
                HasArrived = true;
            }
            else
            {
                HasArrived = false; rigidbody2D.AddForce(transform.up * acceleration);
            }
        }
        else
        {
            rigidbody2D.velocity = Vector2.zero;
        }

        transform.RotateUpTowards2d(targetDirection, RotateSpeed * Time.deltaTime, satisfactionAngles);
	}

    public void MoveTowardsTarget(Vector2 target)
    {
        MoveTowardsTarget(target, () => { });
    }

    public void StopMovement()
    {
        stopped = true;
    }

    public void MoveTowardsTarget(Vector2 target, Action onArrival)
    {
        this.target = target + targetOffset;

        if ((this.target - transform.position2d()).sqrMagnitude < targetSatisfactionSqr)
        {
            HasArrived = true;
            return;
        }

        HasArrived = false;
        stopped = false;
        this.onArrival = onArrival;
    }
}
