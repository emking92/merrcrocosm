﻿using UnityEngine;
using System.Collections;

public class Particle_Emitter : MonoBehaviour {

	public GameObject particle_prefab;
	public float death_delay = .75f;

	public float spawn_delay = .5f;
	private float spawn_timer = 0f;

    public Particles particle;

    public float range;
    private Vector2 lastPosition;

	// Use this for initialization
	void Start () {
        lastPosition = transform.position;
        if (particle == Particles.RisingBubble)
        {
            spawn_delay = Random.Range(.5f, 2f);
        }
	}
	
	// Update is called once per frame
	void Update () {
		spawn_timer += Time.deltaTime;

        if (particle == Particles.MoveBubble)
        {
            Vector2 pos = lastPosition - (Vector2)transform.position;
            float speed = pos.magnitude;
            lastPosition = transform.position;
            float rate;

            if (speed > .25f)
            {
                rate = 25f;
            }
            else
            {
                rate = 25f * speed / .25f;
            }

            spawn_delay = 1f / rate;
        }

        
        

		while (spawn_timer >= spawn_delay){
			spawn_timer -= spawn_delay;
            GameObject particleObject = ResourceFactory<Particles>.Instantiate(particle, transform.position);
            if (particle == Particles.MoveBubble)
            {
                float x = UnityEngine.Random.Range(-range,range);
                float y = UnityEngine.Random.Range(-range,range);
                particleObject.transform.position += new Vector3(x, y, 0f);
            }
            if (particle == Particles.RisingBubble)
            {
                spawn_delay = Random.Range(5f, 20f);
            }
            
			Destroy (particleObject, death_delay);
		}
	}
}

public enum Particles
{
    MoveBubble,
    RisingBubble
}
