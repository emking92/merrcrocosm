﻿using UnityEngine;
using System.Collections;

public class Follower : MonoBehaviour 
{

    public GameObject target;

    public float maxDistance = .5f;
    public float satisfactionRadius = .01f;
    public float revertSpeed = .1f;

	// Update is called once per frame
	void LateUpdate () 
    {
        Vector3 displacement = (target.transform.position - transform.position);
        float distance = displacement.magnitude;

        if (distance > maxDistance)
        {
            displacement = displacement.normalized * maxDistance;
            transform.position = target.transform.position - displacement;
        }
        else if (distance > satisfactionRadius)
        {
            transform.position += displacement.normalized * revertSpeed * Time.deltaTime;
        }
	}
}
