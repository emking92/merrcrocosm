﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class EnemySpawnManager : MonoBehaviour
{
    public Vector2[] SpawnPoints;
    public SpawnPattern[] SpawnPatterns;
    public float SpawnDistance = 5;
    public Vector2 SpawnDirection;
    public float InitialDelay = 6.5f;

    private SpawnPattern currentPattern;
    private float delayTimer;

    private List<GameObject> spawnList;
    private float checkTimer;

    private const float INITIAL_DELAY_VARIANCE = 1.5f;

	// Use this for initialization
	void Start () 
    {
        delayTimer = InitialDelay + UnityEngine.Random.Range(-INITIAL_DELAY_VARIANCE, INITIAL_DELAY_VARIANCE);
        currentPattern = SpawnPatterns.PickRandom();
        spawnList = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (checkTimer <= 0f)
        {
            spawnList.RemoveAll(go => go == null);
            checkTimer = 3f;
        }
        else
        {
            checkTimer -= Time.deltaTime;
        }

        if (spawnList.Count == 0)
        {
            delayTimer -= Time.deltaTime * 3f;
        }

        delayTimer -= Time.deltaTime;

        while (delayTimer < 0)
        {
            Enemies enemy = currentPattern.Enemy;
            GameObject enemyObject = ResourceFactory<Enemies>.Instantiate(enemy, SpawnPoints.PickRandom());
            spawnList.Add(enemyObject);
            enemyObject.transform.SetUpDirection(SpawnDirection);
            enemyObject.GetComponent<Enemy>().Spawn(SpawnDirection, SpawnDistance);

            currentPattern.count--;
            if (currentPattern.count == 0)
            {
                delayTimer += currentPattern.postDelay;
                currentPattern = SpawnPatterns.PickRandom();
            }
            else
            {
                delayTimer += currentPattern.delay;
            }
        }
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        foreach (Vector2 spawn in SpawnPoints)
        {
            Gizmos.DrawWireSphere(spawn, .5f);
            Gizmos.DrawLine(spawn, spawn + SpawnDirection * SpawnDistance);
        }
    }
    
    [Serializable]
    public struct SpawnPattern
    {
        public Enemies Enemy;
        public int count;
        public float delay;
        public int postDelay;
    }
}
