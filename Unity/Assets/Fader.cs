﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour {

    public float fadeTime, fadeSpeed;
    private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (fadeTime > 0f)
        {
            fadeTime -= Time.deltaTime;
        }
        else
        {
            spriteRenderer.color = spriteRenderer.color - Color.black * fadeSpeed * Time.deltaTime;
        }

        if (spriteRenderer.color.a <= 0f)
        {
            GameObject.Destroy(this.gameObject);
        }
	}
}
