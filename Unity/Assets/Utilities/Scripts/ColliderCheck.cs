﻿using UnityEngine;
using System.Collections;

public class ColliderCheck : MonoBehaviour
{
    public LayerMask _layerCheck;

    public bool IsColliding
    {
        get;
        private set;
    }
	
	void FixedUpdate ()
    {
        IsColliding = false;
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        HandleTrigger(collider);
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        HandleTrigger(collider);
    }

    void HandleTrigger(Collider2D collider)
    {
        if (((1 << collider.gameObject.layer) & _layerCheck) != 0)
        {
            IsColliding = true;
        }
    }
}
