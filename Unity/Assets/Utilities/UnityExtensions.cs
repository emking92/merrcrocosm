﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Runtime.Serialization;
using System.Reflection;

[System.Diagnostics.DebuggerNonUserCode]
public static class UnityExtensions 
{
    	
	#region Public Methods

    #region SetPosition2D

    public static void SetPosition2D(this GameObject gameObject, Vector2 position)
    {
        gameObject.transform.position = position;
    }

    public static void SetPosition2D(this GameObject gameObject, float x, float y)
    {
        gameObject.SetPosition2D(new Vector2(x, y));
    }



    #endregion
    
    #region SetParent

    public static void SetParentObject(this GameObject gameObject, GameObject parent, bool applyParentScale)
    {
        if (applyParentScale)
        {
            Vector3 parentScale = parent.transform.lossyScale;
            parentScale.z = 1f;
            parent.SetWorldScale(Vector3.one);

            gameObject.transform.parent = parent.transform;

            parent.SetWorldScale(parentScale);
        }
        else
        {
            gameObject.transform.parent = parent.transform;

            Vector3 scale = gameObject.transform.localScale;
            scale.z = 1;
            gameObject.transform.localScale = scale;
        }
    }

    public static void SetParentObject(this GameObject gameObject, GameObject parent)
    {
        gameObject.SetParentObject(parent, false);
    }

    public static void SetParentObject(this GameObject gameObject, Component parent, bool applyParentScale)
    {
        gameObject.SetParentObject(parent.gameObject, applyParentScale);
    }

    public static void SetParentObject(this GameObject gameObject, Component parent)
    {
        gameObject.SetParentObject(parent, false);
    }

    #endregion

    public static void SetScaleIgnoreChildren(this Transform source, Vector3 scale)
    {
        scale.z = 1f;
        Vector2 current = source.localScale;
        float inverseX = current.x / scale.x;
        float inverseY = current.y / scale.y;

        source.localScale = scale;

        foreach (Transform child in source)
        {
            Vector2 childScale = child.localScale;
            childScale.x *= inverseX;
            childScale.y *= inverseY;
            child.localScale = childScale;
        }
    }

    public static void SetWorldScale(this GameObject gameObject, Vector3 worldScale)
    {
        Transform parent = gameObject.transform.parent;

        if (parent == null)
        {
            gameObject.transform.localScale = worldScale;
        }
        else
        {
            Vector3 parentScale = parent.lossyScale;
            parentScale.z = 1;
            parent.gameObject.SetWorldScale(parentScale);

            gameObject.transform.parent = null;
            gameObject.transform.localScale = worldScale;
            gameObject.transform.parent = parent;
        }
    }

    public static bool OverlapScreenPoint(this Collider2D collider, Vector2 point)
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(point);
        return collider.OverlapPoint(worldPoint);
    }

    public static Vector2 ClampWithin(this Rect source, Rect bounds)
    {
        Vector2 offset = Vector2.zero;

        float rightOffset = bounds.xMax - source.xMax;
        float leftOffset = bounds.xMin - source.xMin;
        float bottomOffset = bounds.yMax - source.yMax;
        float topOffset = bounds.yMin - source.yMin;

        if (rightOffset < 0) offset.x = rightOffset;
        if (leftOffset > 0) offset.x = leftOffset;
        if (bottomOffset < 0) offset.y = bottomOffset;
        if (topOffset > 0) offset.y = topOffset;

        return offset;
    }

    public static Sprite Clone(this Sprite source)
    {
        Texture2D sourceTexture = source.texture;

        Texture2D newTexture = new Texture2D(sourceTexture.width, sourceTexture.height, sourceTexture.format, false);
        newTexture.SetPixels(sourceTexture.GetPixels());
        
        Sprite newSprite = Sprite.Create(newTexture, source.rect, Vector2.one * .5f);
        return newSprite;
    }

    public static Touch[] TouchesWithin(this Collider2D collider2D)
    {
        return Input.touches.Where(t => collider2D.OverlapScreenPoint(t.position))
                     .ToArray();
    }

    public static bool TouchWithin(this Collider2D collider2D, out Touch touch)
    {
        Touch[] touches = collider2D.TouchesWithin();

        if (touches.Any())
        {
            touch = touches[0];
            return true;
        }
        else
        {
            touch = default(Touch);
            return false;
        }            
    }

    public static Vector2 GetWorldPosition(this Touch touch)
    {
        return Camera.main.ScreenToWorldPoint(touch.position);
    }

    public static Vector2 position2d(this Transform transform)
    {
        return transform.position;
    }

    public static Transform FindNested(this Transform target, string name)
    {
        if (target.name == name) return target;

        foreach (Transform child in target)
        {
            var result = FindNested(child, name);

            if (result != null) return result;
        }   

        return null;
    }

    public static T FindChildComponent<T>(this Transform transform, string childName) where T:Component
    {
        return transform.FindChild(childName).GetComponent<T>();
    }

    public static void Rotate2D(this Transform transform, float angles)
    {
        transform.Rotate(0, 0, angles);
    }

    public static float GetRightAngle(this Transform transform)
    {
        Vector2 right = transform.right;
        float degrees = Mathf.Atan2(right.y, right.x) * 180f/ Mathf.PI;
        return (degrees < 0f) ? 360f + degrees : degrees;
    }

    public static void SetRightAngle(this Transform transform, float angles)
    {
        transform.rotation = Quaternion.Euler(0, 0, angles);
    }

    public static void SetUpDirection(this Transform transform, Vector3 direction)
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
    }

	public static Vector2 RotateTowards(this Vector2 current, Vector2 direction, float maxDegrees)
	{
        float sign;
        float angle = current.AngleTo(direction, out sign);

        if (angle <= maxDegrees)
        {
            return direction;
        }
        else
        {
            return Quaternion.Euler(0, 0, maxDegrees * sign) * current;
        }
	}

    public static float AngleToSigned(this Vector2 current, Vector2 direction)
    {
        float sign;
        float angle = current.AngleTo(direction, out sign);

        return angle * sign;
    }

    public static float AngleTo(this Vector2 current, Vector2 direction, out float sign)
    {
        sign = (Vector3.Cross(current, direction).z < 0) ? -1 : 1;
        return Vector2.Angle(current, direction);
    }
    
    public static Vector3 RotateTowards2d(this Vector3 current, Vector2 direction, float maxDegrees)
    {
        return ((Vector2) current).RotateTowards(direction, maxDegrees);
    }

    public static void RotateUpTowards2d(this Transform transform, Vector2 direction, float maxDegrees, float satisfactionAngles)
    {
        if (Vector2.Angle(transform.up, direction) > satisfactionAngles)
            RotateUpTowards2d(transform, direction, maxDegrees);
    }

    public static void RotateUpTowards2d(this Transform transform, Vector2 direction, float maxDegrees)
    {
        transform.SetUpDirection(transform.up.RotateTowards2d(direction, maxDegrees));
    }

    public static bool Contains(this Bounds outerBounds, Bounds bounds)
    {
        return outerBounds.Contains(bounds.min) && outerBounds.Contains(bounds.max);
    }

    public static void CapSpeed(this Rigidbody2D rigidbody, float cap)
    {
        Vector2 velocity = rigidbody.velocity;
        float magnitude = MathUtilities.CapMagnitude(velocity.magnitude, cap);

        rigidbody.velocity = velocity.normalized * magnitude;
    }

    public static Vector2 RandomInnerPoint(this Bounds bounds)
    {
        Vector2 randExtent = bounds.extents;
        randExtent.x *= UnityEngine.Random.Range(-1f, 1f);
        randExtent.y *= UnityEngine.Random.Range(-1f, 1f);

        return (Vector2) bounds.center + randExtent;
    }

    public static bool ContainsLayer(this LayerMask mask, int layer)
    {
        return ((1 << layer) & mask) != 0;
    }
	
	#endregion
		
}
