﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class SystemExtensions 
{
    	
	#region Public Methods

    public static T PickRandom<T>(this IList<T> source)
    {
        int rand = UnityEngine.Random.Range(0, source.Count);
        return source[rand];
    }

    #region MinBy

    public static T MinBy<T>(this IEnumerable<T> source, Func<T, int> selector)
    {
        T min = default(T);
        int minVal = int.MaxValue;

        foreach (T element in source)
        {
            int val = selector(element);

            if (val < minVal)
            {
                min = element;
                minVal = val;
            }
        }

        return min;
    }

    public static T MinBy<T>(this IEnumerable<T> source, Func<T, float> selector)
    {
        T min = default(T);
        float minVal = float.MaxValue;

        foreach (T element in source)
        {
            float val = selector(element);

            if (val < minVal)
            {
                min = element;
                minVal = val;
            }
        }

        return min;
    }

    #endregion 

    #region MaxBy

    public static T MaxBy<T>(this IEnumerable<T> source, Func<T, int> selector)
    {
        T max = default(T);
        int maxVal = int.MinValue;

        foreach (T element in source)
        {
            int val = selector(element);

            if (val > maxVal)
            {
                max = element;
                maxVal = val;
            }
        }

        return max;
    }

    public static T MaxBy<T>(this IEnumerable<T> source, Func<T, float> selector)
    {
        T max = default(T);
        float maxVal = float.MinValue;

        foreach (T element in source)
        {
            float val = selector(element);

            if (val > maxVal)
            {
                max = element;
                maxVal = val;
            }
        }

        return max;
    }

    #endregion

    #region MinsBy
    public static IEnumerable<T> MinsBy<T>(this IEnumerable<T> source, Func<T, int> selector)
    {
        List<T> mins = new List<T>();
        int minVal = int.MaxValue;

        foreach (T element in source)
        {
            int val = selector(element);

            if (val > minVal) continue;

            if (val < minVal)
            {
                mins.Clear();
                minVal = val;
            }
            mins.Add(element);
        }

        return mins;
    }

    public static IEnumerable<T> MinsBy<T>(this IEnumerable<T> source, Func<T, float> selector)
    {
        List<T> mins = new List<T>();
        float minVal = float.MaxValue;

        foreach (T element in source)
        {
            float val = selector(element);

            if (val > minVal) continue;

            if (val < minVal)
            {
                mins.Clear();
                minVal = val;
            }
            mins.Add(element);
        }

        return mins;
    }
    #endregion

    #region MaxsBy
    public static IEnumerable<T> MaxsBy<T>(this IEnumerable<T> source, Func<T, int> selector)
    {
        List<T> maxs = new List<T>();
        int maxVal = int.MinValue;

        foreach (T element in source)
        {
            int val = selector(element);

            if (val < maxVal) continue;

            if (val > maxVal)
            {
                maxs.Clear();
                maxVal = val;
            }
            maxs.Add(element);
        }

        return maxs;
    }

    public static IEnumerable<T> MaxsBy<T>(this IEnumerable<T> source, Func<T, float> selector)
    {
        List<T> maxs = new List<T>();
        float maxVal = float.MinValue;

        foreach (T element in source)
        {
            float val = selector(element);

            if (val < maxVal) continue;

            if (val > maxVal)
            {
                maxs.Clear();
                maxVal = val;
            }
            maxs.Add(element);
        }

        return maxs;
    }
    #endregion

    public static bool ContainsFlag<TEnum>(this TEnum mask, TEnum flag) where TEnum : struct, IConvertible
    {
        int maskValue = (int)(object)mask;
        int flagValue = (int)(object)flag;

        return (maskValue & flagValue) == flagValue;
    }

    #endregion

}
